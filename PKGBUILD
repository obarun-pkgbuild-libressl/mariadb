# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/extra/x86_64/mariadb/
## Maintainer : Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
## Maintainer : Christian Hesse <mail@eworm.de>
#--------------------------------------------------------------------------------------

pkgbase=mariadb
pkgname=('mariadb-libs' 'mariadb-clients' 'mariadb' 'mytop')
pkgver=10.3.14
pkgrel=2
arch=('x86_64')
license=('GPL')
_website="https://mariadb.org/"
pkgdesc='Fast SQL database server, derived from MySQL'
url="https://mirrors.ukfast.co.uk/sites/mariadb/mariadb-$pkgver/source"
source=("$url/mariadb-$pkgver.tar.gz"{,.asc}
    '0001-arch-specific.patch'
    '0004-do-not-break-main-configuration-with-instantiated-one.patch'
    'mariadb.sysusers.conf'
    'mariadb.tmpfile.conf')

makedepends=(
    'boost'
    'bzip2'
    'cmake'
    'jemalloc'
    'libaio'
    'libxml2'
    'lz4'
    'lzo'
    'libressl'
    'zlib'
    'zstd')

#--------------------------------------------------------------------------------------
prepare() {
    cd "$pkgbase-$pkgver"

    ## Arch Linux specific patches:
    ##  * enable PrivateTmp for a little bit more security
    ##  * force preloading jemalloc for memory management
    ##  * fix path to our config
    patch -Np1 < ../0001-arch-specific.patch

    ## do not break main configuration with instantiated one
    ## https://github.com/MariaDB/server/pull/1095
    patch -Np1 < ../0004-do-not-break-main-configuration-with-instantiated-one.patch
}

build() {

    local _cmake_options=(
        -DCMAKE_BUILD_TYPE=release
        -Wno-dev
        -DINSTALL_SYSCONFDIR=/etc/mysql
        -DINSTALL_SYSCONF2DIR=/etc/mysql/my.cnf.d
        -DINSTALL_UNIX_ADDRDIR=/run/mysqld/mysqld.sock
        -DCMAKE_INSTALL_PREFIX=/usr
        -DINSTALL_SCRIPTDIR=bin
        -DINSTALL_INCLUDEDIR=include/mysql
        -DINSTALL_PLUGINDIR=lib/mysql/plugin
        -DINSTALL_SHAREDIR=share
        -DINSTALL_SUPPORTFILESDIR=share/mysql
        -DINSTALL_MYSQLSHAREDIR=share/mysql
        -DINSTALL_DOCREADMEDIR=share/doc/mariadb        
        -DINSTALL_DOCDIR=share/doc/mariadb
        -DINSTALL_MANDIR=share/man
        -DMYSQL_DATADIR=/var/lib/mysql
        -DDEFAULT_CHARSET=utf8mb4
        -DDEFAULT_COLLATION=utf8mb4_unicode_ci
        -DBUILD_CONFIG=mysql_release
        -DENABLED_LOCAL_INFILE=ON
        -DPLUGIN_EXAMPLE=NO
        -DPLUGIN_FEDERATED=NO
        -DPLUGIN_FEEDBACK=NO
        -DWITH_EMBEDDED_SERVER=ON
        -DWITH_EXTRA_CHARSETS=complex
        -DWITH_JEMALLOC=ON
        -DWITH_LIBWRAP=OFF
        -DWITH_PCRE=bundled
        -DWITH_READLINE=ON
        -DWITH_SSL=system
        -DWITH_SYSTEMD=no
        -DWITH_UNIT_TESTS=OFF
        -DWITH_ZLIB=system
    )

    mkdir build
    cd build

    cmake ../"$pkgbase-$pkgver" "${_cmake_options[@]}"
    make
}

#check() {
#   cd build/mysql-test

    ## Takes *really* long, so disabled by default.
    #./mtr --parallel=5 --mem --force --max-test-fail=0
#}

package_mariadb-libs() {
    pkgdesc='MariaDB libraries'
    depends=(
        'bzip2'
        'libaio'
        'lz4'
        'lzo'
        'libressl'
        'xz'
        'zlib')
    conflicts=(
        'libmysqlclient'
        'libmariadbclient'
        'mariadb-connector-c')
    provides=(
        'libmariadbclient'
        'mariadb-connector-c')
    replaces=(
        'libmariadbclient')

    cd build

    for dir in libmariadb libmysqld libservices include; do
     make -C "$dir" DESTDIR="$pkgdir" install
    done

    ln -s mariadb_config "$pkgdir"/usr/bin/mysql_config
    install -Dm0644 "$srcdir"/$pkgbase-$pkgver/man/mysql_config.1 "$pkgdir"/usr/share/man/man1/mysql_config.1

    install -Dm0644 support-files/mariadb.pc "$pkgdir"/usr/share/pkgconfig/mariadb.pc
    install -Dm0644 "$srcdir"/$pkgbase-$pkgver/support-files/mysql.m4 "$pkgdir"/usr/share/aclocal/mysql.m4

    cd "$pkgdir"

    ## remove static libraries
    rm "$pkgdir"/usr/lib/*.a
}

package_mariadb-clients() {
    pkgdesc='MariaDB client tools'
    depends=(
        "mariadb-libs=$pkgver"
        'jemalloc')
    conflicts=(
        'mysql-clients')
    provides=(
        "mysql-clients=$pkgver")

    cd build

    make -C client DESTDIR="$pkgdir" install

    ## install man pages
    for man in mysql mysql_plugin mysql_upgrade mysqladmin mysqlbinlog mysqlcheck mysqldump mysqlimport mysqlshow mysqlslap mysqltest; do
     install -Dm0644 "$srcdir"/$pkgbase-$pkgver/man/$man.1 "$pkgdir"/usr/share/man/man1/$man.1
    done
}

package_mariadb() {
    pkgdesc='Fast SQL database server, derived from MySQL'
    backup=(
        'etc/mysql/my.cnf'
        'etc/mysql/my.cnf.d/client.cnf'
        'etc/mysql/my.cnf.d/enable_encryption.preset'
        'etc/mysql/my.cnf.d/mysql-clients.cnf'
        'etc/mysql/my.cnf.d/server.cnf')

    install=mariadb.install

    depends=(
        "mariadb-clients=$pkgver"
        'inetutils'
        'libxml2'
        'zstd')
    optdepends=(
        'galera: for MariaDB cluster with Galera WSREP'
        'perl-dbd-mysql: for mysqlhotcopy, mysql_convert_table_format and mysql_setpermission')
    conflicts=(
        'mysql')
    provides=(
        "mysql=$pkgver")
    options=(
        'emptydirs')

    cd build

    make DESTDIR="$pkgdir" install

    cd "$pkgdir"

    ## no SysV init, please!
    rm -r etc/mysql/{init.d,logrotate.d}
    rm usr/bin/rcmysql
    rm usr/share/mysql/{binary-configure,mysql{,d_multi}.server}

    ## these should have useful names
    #mv usr/lib/sysusers.d/{sysusers,mariadb}.conf
    #mv usr/lib/tmpfiles.d/{tmpfiles,mariadb}.conf

    install -Dm0644 "${srcdir}"/mariadb.sysusers.conf usr/lib/sysusers.d/mariadb.conf
    install -Dm0644 "${srcdir}"/mariadb.tmpfile.conf usr/lib/tmpfiles.d/mariadb.conf

    ## move to proper licenses directories
    install -d usr/share/licenses/mariadb
    mv usr/share/doc/mariadb/COPYING* usr/share/licenses/mariadb/

    ## move it where one might look for it
    mv usr/share/{groonga{,-normalizer-mysql},doc/mariadb/}

    ## provided by mariadb-libs
    rm usr/bin/mariadb_config
    rm usr/bin/mysql_config
    rm -r usr/include/
    rm usr/share/man/man1/mysql_config.1
    rm -r usr/share/{aclocal,pkgconfig}
    rm usr/lib/lib*
    rm usr/lib/mysql/plugin/{auth_gssapi_client,caching_sha2_password,dialog,mysql_clear_password,sha256_password}.so
    rm -r usr/lib/pkgconfig/

    ## provided by mariadb-clients
    rm usr/bin/{mysql,mysql_plugin,mysql_upgrade,mysqladmin,mysqlbinlog,mysqlcheck,mysqldump,mysqlimport,mysqlshow,mysqlslap,mysqltest}
    rm usr/share/man/man1/{mysql,mysql_plugin,mysql_upgrade,mysqladmin,mysqlbinlog,mysqlcheck,mysqldump,mysqlimport,mysqlshow,mysqlslap,mysqltest}.1

    ## provided by mytop
    rm usr/bin/mytop

    ## not needed
    rm -r usr/{data,mysql-test,sql-bench}
    rm usr/share/man/man1/mysql-test-run.pl.1
}

package_mytop() {
    pkgdesc='Top clone for MariaDB'
    depends=(
        'perl'
        'perl-dbd-mysql'
        'perl-term-readkey')

    cd build
    install -Dm0755 scripts/mytop "$pkgdir"/usr/bin/mytop
}

#--------------------------------------------------------------------------------------
validpgpkeys=('199369E5404BD5FC7D2FE43BCBCB082A1BB943DB' # MariaDB Package Signing Key <package-signing-key@mariadb.org>
)
sha512sums=('39e1a518bb39c62700b9a9997c6d87159fbee4d8ab5ad16a1295b18d05b23b381c2a3f0186918b38662bffb6581ba655d2d9d3a7de0d592c272e827b35679500'
            'SKIP'
            'bd76beab02cf2615bf32543210bbca938885df0c9ccc63eec3ae9d7cefa550fa2686a2145580b805fd93284c2760e29ee7e972d33dab7a8217ade252103abfdf'
            '7bba94f15ae173d7e8c33f5ef142ea8ab50ee640589a607f50abcfdc52c8dfdd928d28c29f79a6e4f8afe0089eb34ea1086c09ddb97dc78edceb642ed60d0d0c'
            '502f9165206ab9df09d77140fd6f2f19fdeae5c201975b64b233a60350919a06bd28044139ec503f23d1c7492bdfc15a097eebef733a44704aa8adb482dbb636'
            '8a711f655e2b8e1a7647e88d2679dd500af54f77d5e206173bb75dbd2bf96ae5608491d07b1a5992722986fffdf68c8f0de79a09c8e539a9e99b8f946e654eeb')
